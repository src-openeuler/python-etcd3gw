%global _empty_manifest_terminate_build 0
Name:           python-etcd3gw
Version:        2.4.2
Release:        1
Summary:        A python client for etcd3 grpc-gateway v3 API
License:        Apache-2.0
URL:            https://github.com/dims/etcd3-gateway
Source0:        https://files.pythonhosted.org/packages/00/56/db0e19678af91d9213cf21c72e7d82a3494d6fc7da16d61c6ba578fd8648/etcd3gw-2.4.2.tar.gz
BuildArch:      noarch

%description
A python client for etcd3 grpc-gateway v3 API

%package -n python3-etcd3gw
Summary:        A python client for etcd3 grpc-gateway v3 API
Provides:       python-etcd3gw
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:  python3-dulwich
# General requires
BuildRequires:  python3-urllib3
BuildRequires:  python3-requests
BuildRequires:  python3-six
BuildRequires:  python3-futurist
# General requires
Requires:       python3-pbr
Requires:       python3-urllib3
Requires:       python3-requests
Requires:       python3-six
Requires:       python3-futurist
%description -n python3-etcd3gw
A python client for etcd3 grpc-gateway v3 API

%package help
Summary:        A python client for etcd3 grpc-gateway v3 API
Provides:       python3-etcd3gw-doc
%description help
A python client for etcd3 grpc-gateway v3 API

%prep
%autosetup -n etcd3gw-%{version} -p1

%build
%py3_build


%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-etcd3gw -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Aug 30 2024 guochao <guochao@kylinos.cn> - 2.4.2-1
- Update package to version 2.4.2
- Remove fallback to time.clock() 

* Wed Aug 21 2024 guochao <guochao@kylinos.cn> - 2.4.1-1
- Update package to version 2.4.1
- Drop extras from test requirements
- Fix watcher failing with huge payloads

* Fri Mar 29 2024 wangqiang <wangqiang1@kylinos.cn> - 2.4.0-1
- Update package to version 2.4.0

* Thu Nov 24 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 2.1.0-1
- Update package to version 2.1.0

* Fri Jul 22 2022 liksh <liks11@chinaunicom.cn> - 1.0.1-1
- Upgrade to 1.0.1 for openstack-Y

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 0.2.5-2
- DESC: delete -S git from %autosetup

* Tue Jul 13 2021 OpenStack_SIG <openstack@openeuler.org> - 0.2.5-1
- Downgrade to 0.2.5
* Tue Feb 23 2021 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
